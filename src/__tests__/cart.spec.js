import React, { useState } from "react";
import ReactDOM from "react-dom";
import {act} from "react-dom/test-utils";

import Cart from "../component/cart/Cart";
let container;

//basic DOM setting
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });
afterEach(() => {
    document.body.removeChild(container);
    container = null;
});



describe("Cart component", () => {
    test("it shows empty list when no product selected", () => {
        act(() => {
            let selectedProducts = [];
            ReactDOM.render(<Cart selectedProducts={selectedProducts} />, container);
        });
        const cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
    });
    test("it shows filled list when list contain selected product", () => {
        act(() => {
            let selectedProducts = [{
                productId:1,
                price:19,
                pieces:2
            }];
            ReactDOM.render(<Cart selectedProducts={selectedProducts} />, container);
        });
        const cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1)
    });
    test("it shows filled list when list contain selected product", () => {
        act(() => {
            let selectedProducts = [{
                productId:1,
                price:19
            },{
                productId:2,
                price:19
            }];
            ReactDOM.render(<Cart selectedProducts={selectedProducts} />, container);
        });
        const cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(2)
    });

    
});
