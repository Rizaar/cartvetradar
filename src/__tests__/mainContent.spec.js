import React from "react";
import ReactDOM from "react-dom";
import {act} from "react-dom/test-utils";

import MainContent from "../component/mainContent/MainContent";
let container;

//basic DOM setting
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });
afterEach(() => {
    document.body.removeChild(container);
    container = null;
    localStorage.clear();
    
});



describe("Cart component", () => {
    test("it shows a catalog and a cart div", () => {
        act(() => {
            ReactDOM.render(<MainContent />, container);
        });
        let catalog = container.getElementsByClassName("catalog");
        let cart = container.getElementsByClassName("cart");
        expect(catalog.length).toBe(1);
        expect(cart.length).toBe(1);
    });
    test("it show an empty cart then when clicked, show an item in the cart", () => {
        act(() => {
            ReactDOM.render(<MainContent />, container);
        });
        let cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
        let product = container.getElementsByClassName("item")[0];
        
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
    });

    test("it show an empty cart then when clicked, show an item in the cart, two click should not ad more than on item if not existant", () => {
        act(() => {
            ReactDOM.render(<MainContent />, container);
        });
        let cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
        let product = container.getElementsByClassName("item")[0];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
    });
    
    test("it show an empty cart then when clicked, show an item in the cart, two click on different products should add two different item", () => {
        act(() => {
            ReactDOM.render(<MainContent />, container);
        });
        let cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
        let product = container.getElementsByClassName("item")[0];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
        product = container.getElementsByClassName("item")[2];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(2);
    });

    test("it show no item, add an item then remove it", () => {
        act(() => {
            ReactDOM.render(<MainContent />, container);
        });
        let cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
        let product = container.getElementsByClassName("item")[0];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
        product = container.getElementsByClassName("remove")[0];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
    });
    test("it show no item, add two items then remove one, one should remain", () => {
        act(() => {
            ReactDOM.render(<MainContent />, container);
        });
        let cartBody = container.getElementsByClassName("cartBody")[0];
        let cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(0);
        let product = container.getElementsByClassName("item")[0];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
        product = container.getElementsByClassName("remove")[0];
        act(()=>{
            product.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        })
        cartBody = container.getElementsByClassName("cartBody")[0];
        cartItems = cartBody.getElementsByClassName("cartItem");
        expect(cartItems.length).toBe(1);
    });
});
