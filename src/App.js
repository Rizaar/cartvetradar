import React from 'react';
import './App.css';
import TopBar from './component/topBar/TopBar';
import MainContent from './component/mainContent/MainContent' ;

function App() {
  return (
    <React.StrictMode>
    <div className="App">
      <TopBar className="App-header"/>
      <MainContent />
    </div>
    </React.StrictMode>

  );
}

export default App;
