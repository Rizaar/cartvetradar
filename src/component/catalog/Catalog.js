import React from 'react';
import './style.css';

export default class Catalog extends React.Component{
    constructor(props){
        super(props);
        this.renderProduct = props.products.map(
            (p)=>
                <div key={p.productId} className="item"  onClick={(e)=>this.addToCart(p,e)}>
                {p.name}<br/> 
                <img src={p.image} alt={p.name} className="catalogImg"></img><br/>
        price : {p.price}$
        </div>);
            
    }

    addToCart = (e) =>{
        this.props.onAddToCart(e);
    }

    render(){
        return (
            <div className="catalog">
                <div>Click on the items to add it in your cart</div>
                <div className="bodyCatalog" >
                     {this.renderProduct}
                </div>  
            </div>
            
        );
    }
};



