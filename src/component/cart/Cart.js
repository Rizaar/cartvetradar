import React from 'react';
import './style.css';

export default class Cart extends React.Component{
    constructor(props){
        super(props);
        this.removeFromCart = this.removeFromCart.bind(this);
    }

    removeFromCart(e) {
        this.props.onRemoveFromCart(e);
    }

    render() {
        const totalAmount = this.props.totalAmount;

        let renderProduct = this.props.selectedProducts.map(
            (p)=>
                <div className="cartItem" key={p.productId} >
                    Pcs : {p.pieces}
                <img src={p.image} alt={p.name} className="cartImg"></img> {p.name} : {p.price} $
                <span className="remove" onClick={(e)=>this.removeFromCart(p,e)}>x</span>
        </div>)
        return (
            <div className="cart">
                <div className="headerCart">Total Amount : {totalAmount} $</div>
                <div className="cartBody">
                    {renderProduct}
                </div>
            </div>
        );
    }
}

