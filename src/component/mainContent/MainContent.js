import React from 'react';
import './style.css';
import Cart from '../cart/Cart'; 
import Catalog from '../catalog/Catalog';

import productsList from '../../products.json' 

export default class MainContent extends React.Component{
    getInitialState (state, defaultValue){
        return JSON.parse(localStorage.getItem(state)) || defaultValue;
    }

    constructor(props){
        super(props);
        this.addToCart = this.addToCart.bind(this);
        this.removeFromCart = this.removeFromCart.bind(this);
        let initialSelectedProducts = this.getInitialState("selectedProducts",[]);
        
        this.state = {
            selectedProducts:initialSelectedProducts ,
            products: productsList,
            totalAmount : this.calcTotal(initialSelectedProducts)
        };
    }

    calcTotal(list){
        let total = 0;
        for(let product of list){
            total += (product.price * product.pieces);
        }
        return total.toFixed(2);
    }

    updateSelectedProduct(list){
        this.setState({selectedProducts : list});
        localStorage.setItem("selectedProducts", JSON.stringify(list));
    }

    addToCart(e){
        let productList = this.state.selectedProducts;
        let item = {...e};
        item.pieces=1;

        let found = false;
        for(let p of productList){
            if(p.productId === item.productId){
                p.pieces +=1;
                found = true;
                break;
            }
        }
        if(found === false){
            productList.push(item);
        }
            
        this.updateSelectedProduct(productList);
        let total = this.calcTotal(productList);
        this.setState({totalAmount:total});
    }
    
    removeFromCart(e){
        for(let p of this.state.selectedProducts){
            if(p.productId === e.productId){
                p.pieces -= 1;
                break;
            }
        }
        let temp = this.state.selectedProducts.filter((e)=>e.pieces >0);
        this.updateSelectedProduct(temp);
        let total = this.calcTotal(temp);
        this.setState({totalAmount:total});
    }

    render(){ 
        const selectedProducts = this.state.selectedProducts;
        const products = this.state.products;
        const totalAmount= this.state.totalAmount;
        return(
            <div className="mainContent">
                <Catalog className="catalog" products={products} onAddToCart={this.addToCart}/>
                <Cart className="cart" selectedProducts={selectedProducts} totalAmount={totalAmount} onRemoveFromCart={this.removeFromCart}/>
            </div>
        );
    } 
}

